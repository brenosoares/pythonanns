import  matplotlib.pyplot  as  plt
import datetime as dt
import  pandas  as  pd
import  numpy  as  np
import tensorflow as tf
from pandas import Series

def show_database(database):

    dates = get_dates(database)

    plt.title('Vazão', fontsize=14) 
    plt.plot(dates, pd.Series(np.ravel(database['Vazao'])), "b", markersize=10, label="Vazão") 
    plt.legend(loc="upper left") 
    plt.xlabel("Anos") 
    plt.show() 

def get_dates(database):

    dates = []
    years = database[:,0:1] 
    months = database[:,1:2] 

    for i in range(0, database.shape[0]):
        dates.append(dt.datetime(year=years[i], month=months[i], day=1))

    return dates

def show_prediction(actual, forecast):   
    
    dates = get_dates(actual)

    plt.title('Forecast vs Actual', fontsize=14) 
    plt.plot(dates, pd.Series(np.ravel(actual[:, actual.shape[1] - 1])), "b", markersize=10, label="Actual") 
    plt.plot(dates, pd.Series(np.ravel(forecast)), "r", markersize=10, label="Forecast")
    plt.legend(loc="upper left") 
    plt.xlabel("Anos") 
    plt.show() 

def mape(y_true,y_pred):
    tot = tf.reduce_sum(y_true)
    #tot = tf.clip_by_value(tot, clip_value_min=1,clip_value_max=1000)
    wmape = tf.realdiv(tf.reduce_sum(tf.abs(tf.subtract(y_true,y_pred))),tot)*100#/tot

    return(wmape)