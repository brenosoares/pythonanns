# Based on: https://machinelearningmastery.com/multivariate-time-series-forecasting-lstms-keras/

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import LSTM
 
import os
import numpy as np

import Util as util
import FileManager as fm

INPUT_DATABASES_FOLDER = 'Databases/'
OUTPUT_FOLDER = 'LSTM/Output/'

OUTPUT_COUNT = 3
EXPERIMENTS = 30
PERCENT_TRAIN = 0.80
PERCENT_TEST = 0.20
SHUFFLE_DATA = False
EPOCHS = 50
BATCH_SIZE = 100
HIDDEN_LAYERS_COUNT = 3
N_NEURONS_LAYER_1 = 512

print("-------Global Configuration------")
print("Output count: " + str(OUTPUT_COUNT))
print("Experiments: " + str(EXPERIMENTS))
print("Percent train: " + str(PERCENT_TRAIN))
print("Percent test: " + str(PERCENT_TEST))
print("Shuffle data: " + str(SHUFFLE_DATA))
print("Epochs: " + str(EPOCHS))

print("Number of neurons in the first layer: " + str(N_NEURONS_LAYER_1))
print("Number of hidden layers: " + str(HIDDEN_LAYERS_COUNT))


np.set_printoptions(suppress=True)

results = fm.find_csv_filenames(INPUT_DATABASES_FOLDER)

for filename in results:

    print("---------------------------------")
    print("Starting process for " + filename)
    print("---------------------------------")
    
    data, row_count, col_count = fm.import_database(INPUT_DATABASES_FOLDER + filename)
    
    print("Setting training and test data...")

    train_count = int(np.floor(row_count * PERCENT_TRAIN))
    print("Train count: " + str(train_count))

    test_count = int(np.ceil(row_count * PERCENT_TEST))
    print("Test count: " + str(test_count))

    train_start = 0
    test_start = train_start + train_count

    train_end = train_start + train_count
    test_end = train_end + test_count
    
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    
    output_indexes = (-1) * OUTPUT_COUNT
    output_range = np.arange(output_indexes, 0)
    
    x_train = data_train[:,:output_indexes]
    x_test = data_test[:,:output_indexes]

    y_train = data_train[:,output_range]
    y_test = data_test[:,output_range]    

    # expected input data shape: (batch_size, timesteps, data_dim)
    x_train = x_train.reshape((x_train.shape[0], 1, x_train.shape[1]))
    x_test = x_test.reshape((x_test.shape[0], 1, x_test.shape[1]))
        
    fm.create_dir(OUTPUT_FOLDER)
     
    output_prediction = []
    output_mape = []

    for j in range(y_test.shape[1]):
        output_prediction.append(y_test[:,j])

    for i in range(EXPERIMENTS):
        print("Running " + str(i + 1) + " of " + str(EXPERIMENTS))

        model = Sequential()

        # input_shape=(timesteps, data_dim) pattern
        model.add(LSTM(N_NEURONS_LAYER_1, input_shape=(x_train.shape[1], x_train.shape[2]), return_sequences=True))
        
        last_layer_neurons_count = N_NEURONS_LAYER_1
        for i in range(0,HIDDEN_LAYERS_COUNT):
            last_layer_neurons_count = last_layer_neurons_count/2
            model.add(LSTM(int(last_layer_neurons_count), return_sequences=True))

        model.add(LSTM(int(OUTPUT_COUNT)))
        #model.add(Dense(OUTPUT_COUNT))

        model.compile(loss='mape', optimizer='adam')

        history = model.fit(x=x_train, y=y_train, epochs=EPOCHS, batch_size=BATCH_SIZE, verbose=2, shuffle=SHUFFLE_DATA)
    
        mape_loss = []
        mape_loss.append(history.history['loss'][-1])
        #mape_loss.append(history.history['val_loss'][-1])
        output_mape.append(mape_loss)
        
        output_data = model.predict(x_test)

        for j in range(output_data.shape[1]):
            output_prediction.append(output_data[:,j])
    
        fm.export_to_csv(output_prediction, OUTPUT_FOLDER + "lstm_prediction_" + filename)
        fm.export_to_csv(output_mape, OUTPUT_FOLDER + "lstm_mape_" + filename)