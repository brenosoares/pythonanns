# Based on: https://medium.com/mlreview/a-simple-deep-learning-model-for-stock-price-prediction-using-tensorflow-30505541d877

print("Starting...")
print("Importing Libraries...")

import tensorflow as tf
import  pandas  as  pd
import  numpy  as  np

import  matplotlib
import os

import Util as util
import FileManager as fm

INPUT_DATABASES_FOLDER = 'Databases/'
OUTPUT_FOLDER = 'DeepMLP/Output/'

OUTPUT_COUNT = 3
EXPERIMENTS = 30
PERCENT_TRAIN = 0.80
PERCENT_TEST = 0.20
SHUFFLE_DATA = False
EPOCHS = 1000

N_NEURONS_LAYER_1 = 1024
N_NEURONS_LAYER_2 = 512
N_NEURONS_LAYER_3 = 256
N_NEURONS_LAYER_4 = 128
LEARNING_RATE = 0.001
SIGMA = 1

print("-------Global Configuration------")
print("Output count: " + str(OUTPUT_COUNT))
print("Experiments: " + str(EXPERIMENTS))
print("Percent train: " + str(PERCENT_TRAIN))
print("Percent test: " + str(PERCENT_TEST))
print("Shuffle data: " + str(SHUFFLE_DATA))
print("Epochs: " + str(EPOCHS))
print("Number of neurons in layer 1: " + str(N_NEURONS_LAYER_1))
print("Number of neurons in layer 2: " + str(N_NEURONS_LAYER_2))
print("Number of neurons in layer 3: " + str(N_NEURONS_LAYER_3))
print("Number of neurons in layer 4: " + str(N_NEURONS_LAYER_4))
print("Learning rate: " + str(LEARNING_RATE))
print("Sigma: " + str(SIGMA))

np.set_printoptions(suppress=True)

results = fm.find_csv_filenames(INPUT_DATABASES_FOLDER)

for filename in results:

    print("---------------------------------")
    print("Starting process for " + filename)
    print("---------------------------------")
    
    data, row_count, col_count = fm.import_database(INPUT_DATABASES_FOLDER + filename)

    if SHUFFLE_DATA:
        np.random.shuffle(data)

    print("Setting training and test data...")

    train_count = int(np.floor(row_count * PERCENT_TRAIN))
    print("Train count: " + str(train_count))

    test_count = int(np.ceil(row_count * PERCENT_TEST))
    print("Test count: " + str(test_count))

    train_start = 0
    test_start = train_start + train_count

    train_end = train_start + train_count
    test_end = train_end + test_count
    
    data_train = data[np.arange(train_start, train_end), :]
    data_test = data[np.arange(test_start, test_end), :]
    
    output_indexes = (-1) * OUTPUT_COUNT
    output_range = np.arange(output_indexes, 0)
    
    x_train = data_train[:,:output_indexes]
    x_test = data_test[:,:output_indexes]

    y_train = data_train[:,output_range]
    y_test = data_test[:,output_range]    

    print("Bulding network model architecture...")

    # Model architecture parameters
    input_columns_count = col_count - OUTPUT_COUNT
    print("Input count: " + str(input_columns_count))

    n_target = OUTPUT_COUNT

    # Placeholder
    X = tf.placeholder(dtype=tf.float32, shape=[None, input_columns_count])
    Y = tf.placeholder(dtype=tf.float32, shape=[None, n_target])

    # Initializers
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=SIGMA)
    bias_initializer = tf.zeros_initializer()

    # Layer 1: Variables for hidden weights and biases
    W_hidden_1 = tf.Variable(weight_initializer([input_columns_count, N_NEURONS_LAYER_1]))
    bias_hidden_1 = tf.Variable(bias_initializer([N_NEURONS_LAYER_1]))
    # Layer 2: Variables for hidden weights and biases
    W_hidden_2 = tf.Variable(weight_initializer([N_NEURONS_LAYER_1, N_NEURONS_LAYER_2]))
    bias_hidden_2 = tf.Variable(bias_initializer([N_NEURONS_LAYER_2]))
    # Layer 3: Variables for hidden weights and biases
    W_hidden_3 = tf.Variable(weight_initializer([N_NEURONS_LAYER_2, N_NEURONS_LAYER_3]))
    bias_hidden_3 = tf.Variable(bias_initializer([N_NEURONS_LAYER_3]))
    # Layer 4: Variables for hidden weights and biases
    W_hidden_4 = tf.Variable(weight_initializer([N_NEURONS_LAYER_3, N_NEURONS_LAYER_4]))
    bias_hidden_4 = tf.Variable(bias_initializer([N_NEURONS_LAYER_4]))

    # Output layer: Variables for output weights and biases
    W_out = tf.Variable(weight_initializer([N_NEURONS_LAYER_4, n_target]))
    bias_out = tf.Variable(bias_initializer([n_target]))

    # Hidden layer
    hidden_1 = tf.nn.relu(tf.add(tf.matmul(X, W_hidden_1), bias_hidden_1))
    hidden_2 = tf.nn.relu(tf.add(tf.matmul(hidden_1, W_hidden_2), bias_hidden_2))
    hidden_3 = tf.nn.relu(tf.add(tf.matmul(hidden_2, W_hidden_3), bias_hidden_3))
    hidden_4 = tf.nn.relu(tf.add(tf.matmul(hidden_3, W_hidden_4), bias_hidden_4))

    # Output layer (must be transposed)
    #out = tf.transpose(tf.add(tf.matmul(hidden_4, W_out), bias_out))
    out = tf.add(tf.matmul(hidden_4, W_out), bias_out)

    # Cost function
    # mse = tf.reduce_mean(tf.squared_difference(out, Y))
    mape = util.mape(Y,out)

    optimizer = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE)		
    training_op = optimizer.minimize(mape)								# train the result of the application of the cost_function 

    init = tf.global_variables_initializer()							# initialize all the variables 
        
    fm.create_dir(OUTPUT_FOLDER)

    output_prediction = []
    output_mape = []

    for j in range(y_test.shape[1]):
        output_prediction.append(y_test[:,j])

    for i in range(EXPERIMENTS):
        print("Running " + str(i + 1) + " of " + str(EXPERIMENTS))
        with tf.Session() as sess: 
            init.run() 
            for ep in range(EPOCHS): 
                sess.run(training_op, feed_dict={X: x_train, Y: y_train}) 
                if ep % 100 == 0: 
                    mape_result = mape.eval(feed_dict={X: x_train, Y: y_train}) 
                    print(ep, "\tMAPE:", mape_result) 

            ypred = sess.run(out, feed_dict={X: x_test, Y: y_test}) 
            
            output_mape.append(mape_result)

            for j in range(ypred.shape[1]):
                output_prediction.append(ypred[:,j])

            fm.export_to_csv(output_prediction, OUTPUT_FOLDER + "deep_mlp_prediction_" + filename)
            fm.export_to_csv(output_mape, OUTPUT_FOLDER + "deep_mlp_mape_" + filename)
    
    